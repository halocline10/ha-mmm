# README #

* HomeAdvisor Service Request Marketing Mix Model 
* Version 0.1

## Purpose ##

### Summary ###
This repository is to build marketing mix model for HomeAdvisor. The dependent variable is the number of total Service Requests generated and should have robust explanatory power which attributes the number of Service Requests to Marketing activities, Operational activities, Competitive Environment, Macro Environment, and Seasonality and/or Trending Business Growth.

### Contents ###
The repository should include scripts documenting the following:

* Loading of source data to be used in the model
* Data pre-processing / clean up steps
* Creation of model features, including feature engineering, transformations, smoothing, etc.
* Exploratory visualizations
* Model creations
* Model evaluations / comparisons

### Getting started ###

* **getSRs.R** -- contains the source data / query for the dependent variable, Service Requests.
* **getMMMDataSources.R** -- Documents the source data for potential model features, including file path, loading steps, and initial data cleansing. The script outputs .Rdata files for each feature candidate.
* **/data** -- Contains the .Rdata files generated by getMMMDataSources.R
* **tmp_model.R** -- Explores a variety of initial model approaches. This should be replaced by something more formal and easier to follow.

### Contribution guidelines ###

* TBD

### Contacts ###

* Matt Glissmann, mglissmann@homeadvisor.com
* Kate Barnes, kbarnes@homeadvisor.com